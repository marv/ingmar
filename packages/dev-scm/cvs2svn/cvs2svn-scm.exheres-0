# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="http://cvs2svn.tigris.org/svn/cvs2svn"
SCM_SVN_USERNAME="guest"

require distutils scm-svn

SUMMARY="A tool for migrating a CVS repository to Subversion or git"
DESCRIPTION="
The main design goals are robustness and 100% data preservation. cvs2svn can
convert just about any CVS repository we've ever seen, including gcc, Mozilla,
FreeBSD, KDE, GNOME...
cvs2svn infers what happened in the history of your CVS repository and
replicates that history as accurately as possible in the target SCM. All
revisions, branches, tags, log messages, author names, and commit dates are
converted. cvs2svn deduces what CVS modifications were made at the same time,
and outputs these modifications grouped together as changesets in the target
SCM. cvs2svn also deals with many CVS quirks and is highly configurable.
"
HOMEPAGE="http://${PN}.tigris.org/"
DOWNLOADS=""

LICENCES="Apache-1.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/python:=[>=2.4][dbm(-)]
    suggestion:
        dev-scm/cvs [[ description = [ Necessary for using 'cvs2svn --use-cvs' ] ]]
        dev-scm/rcs [[ description = [ Necessary for using 'cvs2svn --use-rcs' ] ]]
"

RESTRICT="test"

src_compile() {
    distutils_src_compile
    emake man
}

src_test() {
    "${PYTHON}" run-tests.py || die "run_tests.py returned exit status: $?"
}

src_install() {
    distutils_src_install
    doman *.1
    dodoc *.options
}

